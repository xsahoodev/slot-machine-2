import java.util.ArrayList;


public interface UpdateInterface {
	
	/**
	 * Used to update Model
	 * @param wheels
	 */
	public void update(ArrayList<Wheel> wheels);
	
	/**
	 * Used to update View
	 * @param numOfTokensWon
	 */
	public void update(int numOfTokensWon);
	
	/**
	 * Used to update View & Model when a user clicks spin button; prepares both for a new spin
	 * 
	 */
	public void newSpin();

}
