import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

/**
 * @author Vishal Ravuri, Jonathan Sahoo, Chiu Cheung-Faat, Edgar Reyes
 *
 */
public class SlotMachineDemo {

	private static JMenuItem chooseSkin; 
	
	/**
	 * Sets the GUI components for the dropdown menu which contains the skin selection. 
	 * Sets the GUI components for the textField where the number of wheels is selected. 
	 * Sets the functionality of the play button, executing the slotmachine functions. 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Main Launched");
		
		JFrame initGameWindow = new JFrame("Slot Machine");
		initGameWindow.setSize(900, 125);
		initGameWindow.setLayout(new GridLayout(3,1));
		initGameWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel1 = new JPanel(); 
        JPanel panel2 = new JPanel(); 
        
        JMenuBar bar = new JMenuBar();
        JMenu chooseSkin = new JMenu("Select a Theme"); 
        JMenuItem dSkin = new JMenuItem("Default");
        JMenuItem pokSkin = new JMenuItem("Pokemon");
        chooseSkin.add(dSkin);
        chooseSkin.add(pokSkin); 
        bar.add(chooseSkin); 
        bar.setSize(300, 300);
        JLabel selected = new JLabel("Default");
        dSkin.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				selected.setText("Default");
				
			}
		});
        pokSkin.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				selected.setText("Pokemon");
				
			}
		});
        panel2.add(bar);
        panel2.add(selected);
        
        panel1.add(new JLabel("First, please enter the number of wheels as an integer (invalid input will result in the default creation of 3 wheels):"));
        JTextField textField = new JTextField(5);
        panel1.add(textField);
        initGameWindow.add(panel1);
        initGameWindow.add(panel2); 
        JButton play = new JButton("Play!");
        play.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (selected.getText()=="Default")
				{
					System.out.println("default selected");
					int numOfWheels = 3;
					try {
						numOfWheels = Integer.parseInt(textField.getText());
	                }
	                catch (NumberFormatException error){
	                	numOfWheels = 3;
	                }
					initGameWindow.setVisible(false);
					initGUI("resources/Skin1.txt", numOfWheels);
				}
				else if (selected.getText()=="Pokemon")
				{
					System.out.println("pokemon selected");
					int numOfWheels = 3;
					try {
						numOfWheels = Integer.parseInt(textField.getText());
	                }
	                catch (NumberFormatException error){
	                	numOfWheels = 3;
	                }
					initGameWindow.setVisible(false);
					initGUI("resources/Skin2.txt", numOfWheels);
				}
					
				
			}
		});
        initGameWindow.add(play);
        initGameWindow.setVisible(true);
				
		


	}
	
	/**
	 * Initializes all of the various GUI elements by creating instances of 
	 * all the different class components. 
	 * @param filepath  The file name that is passed into the controller to obtain a skin. 
	 * @param numOfWheels The number of wheels that will be displayed. 
	 */
	public static void initGUI(String filepath, int numOfWheels)
	{
		JFrame myWindow = new JFrame();
		myWindow = new JFrame("Slot Machine");
       myWindow.setSize(1200, 700);
       myWindow.setLocation(100, 50);
       myWindow.getContentPane().setLayout(new BoxLayout(myWindow.getContentPane(), BoxLayout.Y_AXIS));
       myWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       
       

       
       SlotMachineView view = new SlotMachineView();
       myWindow.add(view);
       
       SlotMachineController controller = new SlotMachineController(numOfWheels, filepath);
       myWindow.add(controller);
       
       SlotMachineModel model = new SlotMachineModel(numOfWheels);
       
       model.setView(view);
       controller.setModel(model);
       
       myWindow.setVisible(true);
        
//       view.update(args);
	}

}
