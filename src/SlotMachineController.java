import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * @author Vishal Ravuri, Jonathan Sahoo, Chiu Cheung-Faat, Edgar Reyes
 *
 */
public class SlotMachineController extends JPanel {

	ArrayList<JButton> imageButtons;
	ArrayList<JButton> stopButtons;
	ArrayList<ImageIcon> images;
	ArrayList<Wheel> wheels;
	ArrayList<Integer> imageIndexes;
	private Image background;
	
	private UpdateInterface model;
	
	JPanel imagePanel;
	JPanel stopButtonPanel;
	
	/**
	 * Creates the GUI components for the slot machine wheels. 
	 * Creates the buttons for the slot machine's functionality. 
	 * Contains two different images for the spin button to simulate an animation.
	 * @param numOfWheels The number of wheels that the user has input.
	 * @param filepath The selected skin that is to be loaded. 
	 */
	public SlotMachineController(int numOfWheels, String filepath) {
		this.setLayout(new GridLayout(2,1));
		
		JPanel wheelsPanel = new JPanel();
		wheelsPanel.setOpaque(false);
		JPanel spinButtonPanel = new JPanel();
		spinButtonPanel.setOpaque(false);
		
		imageIndexes = new ArrayList<>();
		
		//set background of jpanel created within jcomponent
//		JPanel spinButtonPanel = new JPanel() {
//            @Override
//            public void paintComponent(Graphics g) {
//                super.paintComponent(g);
//                g.drawImage(background, 0, 0, null);
//            }
//        };
		
//		wheelsPanel.setBackground(Color.BLACK);
		
		this.add(wheelsPanel);
		this.add(spinButtonPanel);
		
		images = new ArrayList<>();
		loadSkin(filepath);
		
		imagePanel = new JPanel();
		stopButtonPanel = new JPanel();
		
		imageButtons = new ArrayList<>();
		stopButtons = new ArrayList<>();
		wheels = new ArrayList<>();
		
		for (int i=numOfWheels; i>0; i--)
		{
			Random rand = new Random();
		    int randomNum = rand.nextInt((images.size() - 1) + 1) + 0;
		    
		    
		    Wheel newWheel = new Wheel(images.get(randomNum), filepath);
		    
		    
		    wheels.add(newWheel);
		    wheelsPanel.add(newWheel);
		    
		    

		}
		
		ImageIcon normalIcon = new ImageIcon("resources/SpinNormal.png");
        ImageIcon pressedIcon = new ImageIcon("resources/SpinPressed.png");

		JButton spinButton = new JButton();
		
        spinButton.setIcon(normalIcon);
        spinButton.addMouseListener(new MouseAdapter()
        {
            public void mousePressed(MouseEvent e) 
            {
                spinButton.setIcon(pressedIcon);
            }

            public void mouseReleased(MouseEvent e) 
            {
                 spinButton.setIcon(normalIcon);
            }
        });
        
		spinButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				model.newSpin();
				for (Wheel wheel : wheels)
					{
						Thread newThread = new Thread(wheel);
						wheel.run = true;
						newThread.start();
						
						wheel.stopButton.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								System.out.println("STOP BUTTON PRESSED");
								wheel.run = false;
								newThread.interrupt();
//								System.out.println("Index: " + wheel.imageIndex);
								imageIndexes.add(wheel.imageIndex);
								
								model.update(wheels);
								
							}
						});
					}
				
			}
		});
		spinButtonPanel.add(spinButton);
		
	}
	
	/**
	 * MVC standard to synergize with model. 
	 * @param newModel
	 */
	public void setModel(UpdateInterface newModel)
	{
		this.model = newModel;
	}
	
    /**
     * Scans file and loads the images to be used in the slot machine skin. 
     * @param filepath The skin to be loaded. 
     */
    public void loadSkin(String filepath) {

        try {
            File file = new File(filepath);
            BufferedReader in = new BufferedReader(new FileReader(file));
            Scanner s = new Scanner(in);
            // find background
            background = getImage(s.nextLine());

            // all thats left is to get the Symbols
            while (s.hasNext()) {
                images.add(new ImageIcon(s.nextLine()));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Reads a file in order to select the specific images contained in said file. 
     *
     * @param file The file in which the images are contained. 
     * @return image The image[s] that are used in the wheel. 
     */
    public BufferedImage getImage(String file) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(file));
        } catch (IOException e) {
            System.out.println(e);
        }
        return image;
    }

//	@Override
//	public void update(boolean... args) {
//		for (Wheel wheel : wheels)
//		{
//			Thread newThread = new Thread(wheel);
//			wheel.run = true;
//			newThread.start();
//			
//			wheel.stopButton.addActionListener(new ActionListener() {
//				
//				@Override
//				public void actionPerformed(ActionEvent e) {
//					System.out.println("STOP BUTTON PRESSED");
//					wheel.run = false;
//					newThread.interrupt();
//					
//				}
//			});
//		}
//		
//	}
	
	@Override
	  protected void paintComponent(Graphics g) {

	    super.paintComponent(g);
	        g.drawImage(background, 0, 0, null);
	}

}
