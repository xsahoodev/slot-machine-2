import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;


/**
 * @author Vishal Ravuri, Jonathan Sahoo, Chiu Cheung-Faat, Edgar Reyes
 *
 */
public class Wheel extends JPanel implements Runnable{

	ArrayList<ImageIcon> images;
	JButton imageButton;
	JButton stopButton;
	int imageIndex = -1;
	boolean run;
	
	/**
	 * Sets the images that will be depicted by the wheel. Creates buttons
	 * that will be used to stop and start the spinning process. 
	 *  
	 * @param image THe image that will be loaded into the wheel. 
	 * @param filepath The file from where said image will be taken. 
	 */
	public Wheel(ImageIcon image, String filepath) {
		super();
		
		images = new ArrayList<>();
		loadSkin(filepath);

		this.setLayout(new GridLayout(2,1));
		this.setSize(100,100);
		
		imageButton = new JButton();
		imageButton.setIcon(image);
//		imageButton.setEnabled(false);
		imageButton.setPreferredSize(new Dimension(100,100));
		this.add(imageButton);
		
		stopButton = new JButton("Stop");
		stopButton.setPreferredSize(new Dimension(100, 50));
//		stopButton.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				System.out.println("STOP BUTTON PRESSED");
//				
//			}
//		});
		this.add(stopButton);
		
		
	}

	@Override
	public void run() {
		
		while(run) {

		
			try {
			    Thread.sleep(100);
		
				Random rand = new Random();
			    int randomNum = rand.nextInt((images.size() - 1) + 1) + 0;
			    
			    imageIndex = randomNum;
			    
				imageButton.setIcon(images.get(randomNum));
	
			} catch (InterruptedException e) {
//			    e.printStackTrace();
			    Thread.currentThread().interrupt();
			    run = false;
			}
		}
		
	}


	/**
	 * Reads through a file of numerous images and scans them for implementation 
	 * as the wheel images. 
	 * @param filepath The file that contains a list of images. 
	 */
public void loadSkin(String filepath) {
	
  try {
      File file = new File(filepath);
      BufferedReader in = new BufferedReader(new FileReader(file));
      Scanner s = new Scanner(in);
      // find background
      String ignoreFirstLine = s.nextLine();

      // all thats left is to get the Symbols
      while (s.hasNext()) {
          images.add(new ImageIcon(s.nextLine()));
      }

  } catch (FileNotFoundException e) {
      e.printStackTrace();
  }
}

/**
* Reads the image file and selects one specific image to be returned. 
*
* @param file The file that contains the list of images. 
* @return image Returns a single image from the file. 
*/
public BufferedImage getImage(String file) {
  BufferedImage image = null;
  try {
      image = ImageIO.read(new File(file));
  } catch (IOException e) {
      System.out.println(e);
  }
  return image;
}
}
