import java.util.ArrayList;
import java.util.Random;

/**
 * @author Vishal Ravuri, Jonathan Sahoo, Chiu Cheung-Faat, Edgar Reyes
 */
public class SlotMachineModel implements UpdateInterface
{

	private ArrayList<UpdateInterface> views;
	private ArrayList<Wheel> wheels;
    private UpdateInterface view;
    
    private int numberOfSlots;
	private int numberOfSymbols;
	
	private ArrayList<Integer> imageIndexes;
	private int tokenFiled;
	private int win;
	private int tokenFiledWon;
	
	/**
	 * Constructor sets the number of wheels and sets the image ArrayList.
	 * 
	 * @param numOfWheels The number of wheels that will be shown. 
	 */

	public SlotMachineModel(int numOfWheels) 
	{
		super();
		this.numberOfSlots = numOfWheels;
		imageIndexes = new ArrayList<>();
//		this.numberOfSymbols = Setting.numberOfSymbols;
//		this.tokenFiled = Setting.numberOfPlays;
//		imageIndexes = new int[numberOfSlots];
	}
	
    /**
     * MVC Standard to synergize with view. 
     * 
     * @param newView The view class that will be implemented. 
     */
    public void setView(UpdateInterface newView)
    {
        this.view = newView;
    }
    
    /**
     * Cycles the symbols that are displayed inside the wheels. 
     * @return 
     */
    public int getSymbols()
	{
		Random random = new Random();
		return ((random.nextInt(100)) % this.numberOfSymbols);
	}
    
    /**
     * Checks the wheels in order to determine if the user won, also 
     * amends the token count. 
     */
    public void checkWin()
	{
		int max = 1;
		int index = 0;
		for(int i = 0; i < imageIndexes.size()-1; i++)
		{
			int count = 1;
			for(int j = i+1; j < imageIndexes.size(); j++)
			{
				if(imageIndexes.get(i) == imageIndexes.get(j))
					count++;
			}
			if(max < count)
			{
				max = count;
				index = imageIndexes.get(i);
			}
		}
		if(max > 1)
		{
			setTokenFiledWon(max * index);
			this.setTokenFiled(getTokenFiled() + getTokenFiledWon());
            if(index > 0)
            {
            	this.setWin(win+1);
            }
        }
	}
    
    /**
     * Getters and setters for ease of access. 
     */
 	public void setNumberOfSlots(int numberOfSlots) { this.numberOfSlots = numberOfSlots; }
 	public void setNumberOfSymbols(int numberOfSymbols) { this.numberOfSymbols = numberOfSymbols; }
 	public void setTokenFiled(int tokenFiled) { this.tokenFiled = tokenFiled; }
 	public void setWin(int win){ this.win = win; }
 	public void setTokenFiledWon(int tokenFiledWon) { this.tokenFiledWon = tokenFiledWon; }
    
 	public int getNumberOfSlots(){ return numberOfSlots; }
 	public int getNumberOfSymbols(){ return numberOfSymbols; }
 	public int getTokenFiled(){ return tokenFiled; }
 	public int getWin(){ return win; }
 	public int getTokenFiledWon() { return tokenFiledWon; }
 	
 	//public int[] getResult() { return this.result; }

	@Override
	public void update(ArrayList<Wheel> wheels) 
	{
		boolean allWheelsFinishedSpinning = true;
		for (Wheel wheel : wheels)
		{
			if (wheel.run)
				allWheelsFinishedSpinning = false;
//			else
//				imageIndexes.add(wheel.imageIndex);
		}
		
		if (allWheelsFinishedSpinning)
		{
			System.out.println("ALL WHEELS FINISHED SPINNING");
			imageIndexes.clear();
			for (Wheel wheel : wheels)
			{
				imageIndexes.add(wheel.imageIndex);
//				System.out.println("Index: " + wheel.imageIndex);
			}
			System.out.println(imageIndexes);
			//once all wheels finish spinning, do calculations

			checkWin();
			System.out.println("tokenFiledWon: " + tokenFiledWon);
			view.update(tokenFiledWon);
		}
	}

	@Override
	public void update(int numOfTokensWon) 
	{
		//NOT USED IN THIS CLASS
	}

	@Override
	public void newSpin() {
		view.newSpin();
		
	}

}
