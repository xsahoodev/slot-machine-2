import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Vishal Ravuri, Jonathan Sahoo, Chiu Cheung-Faat, Edgar Reyes
 *
 */
public class SlotMachineView extends JPanel implements UpdateInterface {

	JLabel numberOfTokens;
	JLabel winOrLoseText;
	int numberOfTotalTokens = 10;
	boolean ableToUpdateLabel;
	
	/**
	 * Sets labels for the number of tokens, as well as displaying a win or lose graphic. 
	 */
	public SlotMachineView() {
		super();
		
		this.setLayout(new GridLayout(2,1));
//		this.setBackground(Color.RED);
		numberOfTokens = new JLabel("Tokens: 10");
		numberOfTokens.setFont(new Font("Serif", Font.PLAIN, 50));
		numberOfTokens.setHorizontalAlignment(JLabel.CENTER);
		
		winOrLoseText = new JLabel();
		winOrLoseText.setFont(new Font("Serif", Font.PLAIN, 50));
		winOrLoseText.setHorizontalAlignment(JLabel.CENTER);

		this.add(numberOfTokens);
		this.add(winOrLoseText);
	}

	@Override
	public void update(ArrayList<Wheel> wheels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(int numOfTokensWon) {
		System.out.println("numOfTokensWon: " + numOfTokensWon);
		if (numOfTokensWon==0)
		{
			winOrLoseText.setText("You Lose! You Didn't Win Any Tokens!");
		}
		else
		{
			if (ableToUpdateLabel)
			{
				System.out.println("numberOfTotalTokens before: " + numberOfTotalTokens);
				System.out.println("numOfTokensWon before: " + numOfTokensWon);
				System.out.println("numberOfTotalTokens + numOfTokensWon " + (numberOfTotalTokens+numOfTokensWon));
				
				
				winOrLoseText.setText("You Won " + numOfTokensWon + " Tokens!");
				int temp = numberOfTotalTokens;
				numberOfTotalTokens = numOfTokensWon + temp;
				numberOfTokens.setText("Tokens: "+numberOfTotalTokens);
				ableToUpdateLabel = false;
			}
			
		}
		
		
		
	}

	@Override
	public void newSpin() {
		numberOfTotalTokens--;
		numberOfTokens.setText("Tokens: "+numberOfTotalTokens);
		ableToUpdateLabel = true;
	}



	


}
